import { Module } from '@nestjs/common';
import { ChatAgentModule } from './chat-agent/chat-agent.module';
import { ContactModule } from './contact/contact.module';
import { ConversationModule } from './conversation/conversation.module';
import { ProfilownerModule } from './profilowner/profilowner.module';
import { TeamsModule } from './teams/teams.module';
import { agent } from 'supertest';

@Module({
  imports: [ ChatAgentModule,ContactModule, ConversationModule, ProfilownerModule, TeamsModule]
})
export class DomainModule {}
