import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthorisationModule } from './Authorisation/authorisation.module';
import { ApiModule } from './API/api.module';
import { DatabaseModule } from './Database/database.module';
import { DomainModule } from './Domain/domain.module';
import { PersistenceModule } from './Persistence/persistence.module';

import { UtilsModule } from './Utils/utils.module';

@Module({
  imports: [AuthorisationModule, ApiModule, DatabaseModule, DomainModule, PersistenceModule, UtilsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
