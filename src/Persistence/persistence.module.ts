import { Module } from '@nestjs/common';
import { ConversationModule } from './Conversation/conversationRepository.module';
import {  ChatAgentModule } from './chat-agent/chat-agent-Repository.module';
import {  TeamsModule } from './Teams/TeamsRepository.module';
import {  ContactModule } from './Contact/ContactRepository.module';
import {  ProfilOwnerModule } from './Profil-owner/profil-owner-repository.module';

@Module({
  imports: [ConversationModule,ChatAgentModule, ContactModule,TeamsModule,ProfilOwnerModule]
})
export class PersistenceModule {}
